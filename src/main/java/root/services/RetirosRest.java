package root.services;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.dao.RetiroDAO;
import root.dao.exceptions.NonexistentEntityException;
import root.persistence.entities.Retiro;

@Path("/retiros")
public class RetirosRest {

    RetiroDAO dao = new RetiroDAO();
    Retiro ret = new Retiro();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {

        List<Retiro> lista = dao.findRetiroEntities();

        return Response.ok(200).entity(lista).build();
    }

    /**
     *
     * @param recbuscar
     * @return
     */
    @GET
    @Path("/{recbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarRec(@PathParam("recbuscar") Integer recbuscar) {

        ret = dao.findRetiro(recbuscar);
        return Response.ok(200).entity(ret).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String nuevoRet(Retiro retiro) throws Exception {
        dao.create(retiro);
        return "Creacion del Retiro exitosa";
    }
    
    @PUT
    public Response actualizarRet(Retiro retUpdate) throws Exception{
        String resultado = "Datos actualizados del Retiro";
        dao.edit(retUpdate);
        return Response.ok(resultado).build();
    }
    
    @DELETE
    @Path("/{recdel}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    public Response delRec(@PathParam("recdel") Integer recdel) throws NonexistentEntityException {
         
        ret = dao.findRetiro(recdel);
        dao.destroy(recdel);
        return Response.ok("Retiro "+ret.getRetNombre()+" eliminado exitosamente").build();
    }
}
