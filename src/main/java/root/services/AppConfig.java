
package root.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author SaitamaSensei
 */

@ApplicationPath("api")
public class AppConfig extends Application{
    
}
