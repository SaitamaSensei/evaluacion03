/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SaitamaSensei
 */
@Entity
@Table(name = "retiros")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Retiro.findAll", query = "SELECT r FROM Retiro r"),
    @NamedQuery(name = "Retiro.findByRetRecogedor", query = "SELECT r FROM Retiro r WHERE r.retRecogedor = :retRecogedor"),
    @NamedQuery(name = "Retiro.findByRetNombre", query = "SELECT r FROM Retiro r WHERE r.retNombre = :retNombre"),
    @NamedQuery(name = "Retiro.findByRetCuenta", query = "SELECT r FROM Retiro r WHERE r.retCuenta = :retCuenta"),
    @NamedQuery(name = "Retiro.findByRetDireccion", query = "SELECT r FROM Retiro r WHERE r.retDireccion = :retDireccion")})
public class Retiro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ret_recogedor")
    private Integer retRecogedor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ret_nombre")
    private String retNombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ret_cuenta")
    private int retCuenta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ret_direccion")
    private String retDireccion;

    public Retiro() {
    }

    public Retiro(Integer retRecogedor) {
        this.retRecogedor = retRecogedor;
    }

    public Retiro(Integer retRecogedor, String retNombre, int retCuenta, String retDireccion) {
        this.retRecogedor = retRecogedor;
        this.retNombre = retNombre;
        this.retCuenta = retCuenta;
        this.retDireccion = retDireccion;
    }

    public Integer getRetRecogedor() {
        return retRecogedor;
    }

    public void setRetRecogedor(Integer retRecogedor) {
        this.retRecogedor = retRecogedor;
    }

    public String getRetNombre() {
        return retNombre;
    }

    public void setRetNombre(String retNombre) {
        this.retNombre = retNombre;
    }

    public int getRetCuenta() {
        return retCuenta;
    }

    public void setRetCuenta(int retCuenta) {
        this.retCuenta = retCuenta;
    }

    public String getRetDireccion() {
        return retDireccion;
    }

    public void setRetDireccion(String retDireccion) {
        this.retDireccion = retDireccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (retRecogedor != null ? retRecogedor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Retiro)) {
            return false;
        }
        Retiro other = (Retiro) object;
        if ((this.retRecogedor == null && other.retRecogedor != null) || (this.retRecogedor != null && !this.retRecogedor.equals(other.retRecogedor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.Retiro[ retRecogedor=" + retRecogedor + " ]";
    }
    
}
