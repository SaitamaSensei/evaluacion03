<%-- 
    Document   : index
    Created on : 08-05-2020, 0:33:28
    Author     : SaitamaSensei
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Nombre: Daniel Espíndola</h1><br>
        <h1>Curso: IC107-50.</h1><br>
        
        <h2>GET</h2><br>
        <h3>localhost:8080/Retiros-Rest-1.0/api/retiros</h3><br>
        <h4>En esta URL se muestra la lista completa de la Base de Datos creada en PostgreSQL</h4><br>
        <h2>GET</h2><br>
        <h3>localhost:8080/Retiros-Rest-1.0/api/retiros/{recbuscar}</h3><br>
        <h4>En esta URL se muestra la tupla de la tabla creada mediante la PK correspondiente al codigo del retiro en {recbuscar}</h4><br>
        <h2>POST</h2><br>
        <h3>localhost:8080/Retiros-Rest-1.0/api/retiros</h3><br>
        <h4>En esta URL se puede crear un nuevo retiro o tupla de la tabla ingresando los datos correspondientes</h4><br>
        <h2>PUT</h2><br>
        <h3>localhost:8080/Retiros-Rest-1.0/api/retiros</h3><br>
        <h4>En esta URL se pueden modificar los datos de la tupla seleccionada</h4><br>
        <h2>DELETE</h2><br>
        <h3>localhost:8080/Retiros-Rest-1.0/api/retiros/{recdel}</h3><br>
        <h4>En esta URL se pueden modificar los datos de la tupla seleccionada ingresando la PK en {recdel}</h4><br>
    </body>
</html>
